#!/bin/bash

echo "genimmo.genest.lu" >&2

COOKIES=$(mktemp)

curl -c ${COOKIES} -b ${COOKIES} -L 'http://genimmo.genest.lu/FR/Table_OBJ.php' -s >/dev/null

LISTE="$(curl -b ${COOKIES} -L 'http://genimmo.genest.lu/FR/Table_OBJ.php' --data 'WD_JSON_PROPRIETE_=&WD_BUTTON_CLICK_=A3&WD_ACTION_=&A6=-1&A6_DEB=1&_A6_OCC=0' -s | grep 'Table_OBJ.php?A21&A6=' | sed -e 's/^.*Table_OBJ/http:\/\/genimmo.genest.lu\/FR\/Table_OBJ/' -e 's/" target.*$//' )" # > ${PAGE} #|sed -e '/Table_OBJ.php?A21&A6=/!d' > ${PAGE}

for i in ${LISTE}; do

    curl -b ${COOKIES} -sI "${i}" | grep "^Location" | sed -e 's/^Location: /http:\/\/genimmo.genest.lu\/FR\//'

done

rm -f ${COOKIES}
exit 0
