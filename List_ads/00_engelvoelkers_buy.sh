#!/bin/bash

echo "Engelvoelkers.com" >&2

ADS_PER_PAGE=16
INDEX=${ADS_PER_PAGE}

RESULTATS=$(mktemp)
PAGE=$(mktemp)

curl 'https://www.engelvoelkers.com/fr/search/?q=&startIndex=0&businessArea=residential&sortOrder=DESC&sortField=newestProfileCreationTimestamp&pageSize=18&facets=bsnssr%3Aresidential%3Bcntry%3Aluxembourg%3Btyp%3Abuy%3B' -s -o ${PAGE}

sed -i -e 's/\/div>/\/div>\n/g' ${PAGE}

NB_ADS="$(sed -e '/ev-search-result-title/!d' -e 's/\(\/[[:alnum:]]*>\)/\1\n/g' ${PAGE} | grep "h1>" | sed -e 's/^[[:space:][:alpha:]]*\([[:digit:]]\+\).*$/\1/' )"
REST="$((${NB_ADS}-${ADS_PER_PAGE}))"

sed -e '/ev-teaser-icon ev-watchlist-icon inactive/!d' -e 's/^.*(this, event, {/{/' -e 's/ );">.*$//' ${PAGE} >>${RESULTATS}

while [ ${REST} -gt 0 ]; do
    echo -n -e "\r${INDEX}/${NB_ADS}" >&2
    curl "https://www.engelvoelkers.com/fr/search/?q=&startIndex=${INDEX}&businessArea=residential&sortOrder=DESC&sortField=newestProfileCreationTimestamp&pageSize=18&facets=bsnssr%3Aresidential%3Bcntry%3Aluxembourg%3Btyp%3Abuy%3B" -s -o ${PAGE}
    INDEX=$((${INDEX}+${ADS_PER_PAGE}))
    REST="$((${REST}-${ADS_PER_PAGE}))"
    sed -i -e 's/\/div>/\/div>\n/g' ${PAGE}
    sed -e '/ev-teaser-icon ev-watchlist-icon inactive/!d' -e 's/^.*(this, event, {/{/' -e 's/ );">.*$//' ${PAGE} >>${RESULTATS}
done
echo -n -e "\r" >&2
sed -i -e "s/?.*$//g" -e "s/', imageUrl_: '/ /" -e "s/', subid_: '/ /" -e "s/{ id_: '//" ${RESULTATS}
sed -i -e "s/images\/[[:alnum:]]\+-[[:alnum:]]\+-[[:alnum:]]\+-[[:alnum:]]\+-[[:alnum:]]\+/fr-lu\/bien-immobilier/" ${RESULTATS} 

sed -i -e 's/\([[:digit:]]*\) \([[:digit:]]*\) \(.*$\)/\3-\1\.\2_exp\//' ${RESULTATS}

sed -i -e 's/%C3%A0/a/g' -e 's/%C3%A8/e/g' -e 's/%C3%A9/e/g' -e 's/%C3%B4/o/g' -e 's/%C2%B2/2/g' -e 's/%C3%AA/e/g' -e 's/%C3%BC/ue/g'  ${RESULTATS}
sed -i -e 's/%[[:alnum:]][[:alnum:]]/_/g' ${RESULTATS}

cat ${RESULTATS} | sort | uniq

rm -f ${PAGE} ${RESULTATS}
