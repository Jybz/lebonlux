#!/bin/bash

echo "property.lu" >&2

PAGE=$(mktemp)
RESULTATS=$(mktemp)


echo -n -e "\rx/x..." >&2
curl 'https://www.property.lu/fr/residentiel/a-vendre' -s -o ${PAGE}
# curl 'https://www.property.lu/fr/residentiel/a-louer/1' -s -o ${PAGE}


NB_ADS=$(grep 'mainheadernav_itemlist_itemlink' ${PAGE} | grep '/residentiel/a-vendre' | sed -e 's/^.*>\([[:digit:]]*\)<\/sup>.*$/\1/' )

grep 'class="link3"' ${PAGE} | sed -e 's/^.*<a href="/https:\/\/www.property.lu/' -e 's/" class=.*$//' >${RESULTATS}

# PAGE_INDEX=1
# CURRENT_ADS=$(cat ${RESULTATS} | wc -l)

# while [ ${CURRENT_ADS} -lt ${NB_ADS} ]; do
#     PAGE_INDEX=$((${PAGE_INDEX}+1))
#     echo -n -e "\r${CURRENT_ADS}/${NB_PAGE}..." >&2
#     curl "https://www.property.lu/fr/residentiel/a-louer/${PAGE_INDEX}?search[type][]=85&search[type][]=73&search[bedrooms]=0%3B10&search[surface]=0%3B1000&search[price]=0%3B7000" -s -o ${PAGE}
#     grep 'class="link3"' ${PAGE} | sed -e 's/^.*<a href="/https:\/\/www.property.lu/' -e 's/" class=.*$//' >>${RESULTATS}
# done
# echo -e "\rPage ${CURRENT_ADS}/${NB_PAGE}..." >&2


NB_PAGE=$(grep "pagenav_linklast" ${PAGE} | sed -e 's/^.*a-vendre\/\([[:digit:]]*\)">.*$/\1/')

if [ -z "${NB_PAGE}" ]; then
    echo -e "\r1/1      " >&2
else
    for i in $(seq 2 ${NB_PAGE}); do
        echo -n -e "\rPage ${i}/${NB_PAGE}..." >&2
        curl "https://www.property.lu/fr/residentiel/a-vendre/${i}" -s -o ${PAGE}
        grep 'class="link3"' ${PAGE} | sed -e 's/^.*<a href="/https:\/\/www.property.lu/' -e 's/" class=.*$//' >>${RESULTATS}
    done
    echo -e "\rPage ${i}/${NB_PAGE}..." >&2
fi


cat ${RESULTATS}

rm -f ${PAGE} ${RESULTATS}
exit 0
