#!/bin/bash

echo "carre.lu" >&2

#http://www.carre.lu/en/new/apartments/
#http://www.carre.lu/en/new/houses/
#http://www.carre.lu/en/existing/apartments/
#http://www.carre.lu/en/existing/houses/

PAGE=$(mktemp)
TEMP=$(mktemp)

echo "New appartements..." >&2
curl 'http://www.carre.lu/en/new/apartments/' -s -o ${PAGE}

sed -i -e 's/^[[:space:]]*//' ${PAGE}
sed -i -e '/^[[:space:]]*$/d' ${PAGE}
cat ${PAGE} | tr '\n' ' ' | tr '\r' ' ' >${TEMP}

# sed -i -e 's/<li class="col-lg-4 col-md-6" style="max-height: 300px">/\n/g' ${TEMP} 
sed -i -e 's/<div class="property-item primary-tooltips">/\n<div class="property-item primary-tooltips">/g' ${TEMP}
sed -i -e '/<div class="property-item primary-tooltips">/!d' ${TEMP} 

sed -i -e 's/<\/a>[[:space:]]*<\/div>[[:space:]]*<\/div>[[:space:]]*<\/div>[[:space:]]*<\/li>.*$//' ${TEMP} 

sed -i -e 's/^<div class="property-item primary-tooltips"> <a href="//' -e 's/"> <figure.*$//' ${TEMP}

cat ${TEMP}
echo ""

echo "Old appartements..." >&2
curl 'http://www.carre.lu/en/existing/apartments/' -s -o ${PAGE}

sed -i -e 's/^[[:space:]]*//' ${PAGE}
sed -i -e '/^[[:space:]]*$/d' ${PAGE}
cat ${PAGE} | tr '\n' ' ' | tr '\r' ' ' >${TEMP}

# sed -i -e 's/<li class="col-lg-4 col-md-6" style="max-height: 300px">/\n/g' ${TEMP} 
sed -i -e 's/<div class="property-item primary-tooltips">/\n<div class="property-item primary-tooltips">/g' ${TEMP}
sed -i -e '/<div class="property-item primary-tooltips">/!d' ${TEMP} 

sed -i -e 's/<\/a>[[:space:]]*<\/div>[[:space:]]*<\/div>[[:space:]]*<\/div>[[:space:]]*<\/li>.*$//' ${TEMP} 

sed -i -e 's/^<div class="property-item primary-tooltips"> <a href="//' -e 's/"> <figure.*$//' ${TEMP}

cat ${TEMP}
echo ""

echo "New houses..." >&2
curl 'http://www.carre.lu/en/new/houses/' -s -o ${PAGE}

sed -i -e 's/^[[:space:]]*//' ${PAGE}
sed -i -e '/^[[:space:]]*$/d' ${PAGE}
cat ${PAGE} | tr '\n' ' ' | tr '\r' ' ' >${TEMP}

# sed -i -e 's/<li class="col-lg-4 col-md-6" style="max-height: 300px">/\n/g' ${TEMP} 
sed -i -e 's/<div class="property-item primary-tooltips">/\n<div class="property-item primary-tooltips">/g' ${TEMP}
sed -i -e '/<div class="property-item primary-tooltips">/!d' ${TEMP} 

sed -i -e 's/<\/a>[[:space:]]*<\/div>[[:space:]]*<\/div>[[:space:]]*<\/div>[[:space:]]*<\/li>.*$//' ${TEMP} 

sed -i -e 's/^<div class="property-item primary-tooltips"> <a href="//' -e 's/"> <figure.*$//' ${TEMP}

cat ${TEMP}
echo ""

echo "Old houses..." >&2
curl 'http://www.carre.lu/en/existing/houses/' -s -o ${PAGE}

sed -i -e 's/^[[:space:]]*//' ${PAGE}
sed -i -e '/^[[:space:]]*$/d' ${PAGE}
cat ${PAGE} | tr '\n' ' ' | tr '\r' ' ' >${TEMP}

# sed -i -e 's/<li class="col-lg-4 col-md-6" style="max-height: 300px">/\n/g' ${TEMP} 
sed -i -e 's/<div class="property-item primary-tooltips">/\n<div class="property-item primary-tooltips">/g' ${TEMP}
sed -i -e '/<div class="property-item primary-tooltips">/!d' ${TEMP} 

sed -i -e 's/<\/a>[[:space:]]*<\/div>[[:space:]]*<\/div>[[:space:]]*<\/div>[[:space:]]*<\/li>.*$//' ${TEMP} 

sed -i -e 's/^<div class="property-item primary-tooltips"> <a href="//' -e 's/"> <figure.*$//' ${TEMP}

cat ${TEMP}
echo ""

rm -f ${PAGE} ${TEMP}
exit 0



