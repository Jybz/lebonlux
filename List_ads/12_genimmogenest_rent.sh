#!/bin/bash

echo "genimmo.genest.lu" >&2

COOKIES=$(mktemp)

curl -c ${COOKIES} -b ${COOKIES} -L 'http://genimmo.genest.lu/FR/Table_OBJ.php' -s >/dev/null

LISTE="$(curl -b ${COOKIES} -L 'http://genimmo.genest.lu/FR/Table_OBJ.php' --data 'WD_JSON_PROPRIETE_=&WD_BUTTON_CLICK_=A4&WD_ACTION_=&A6=1&A6_DEB=1&_A6_OCC=30&zrl_1_A12=2019120311-1&zrl_2_A12=201912031&zrl_3_A12=20191203&zrl_4_A12=20191115&zrl_5_A12=20190909&zrl_6_A12=2019080801&zrl_7_A12=20190808&zrl_8_A12=20190724&zrl_9_A12=20190620-05&zrl_10_A12=20180724-1&zrl_11_A12=20180507&zrl_12_A12=20180506&zrl_13_A12=20191015&zrl_14_A12=20190709&zrl_15_A12=20190521-05&zrl_16_A12=20190507&zrl_17_A12=20190206&zrl_18_A12=20190129&zrl_19_A12=20190122&zrl_20_A12=20181210&zrl_21_A12=20181010&zrl_22_A12=20180724&zrl_23_A12=201805312&zrl_24_A12=201805311&zrl_25_A12=20180529&zrl_26_A12=20180511&zrl_27_A12=20180505&zrl_28_A12=20180420&zrl_29_A12=201909251&zrl_30_A12=20190925' -s | grep 'Table_OBJ.php?A21&A6=' | sed -e 's/^.*Table_OBJ/http:\/\/genimmo.genest.lu\/FR\/Table_OBJ/' -e 's/" target.*$//' )" # > ${PAGE} #|sed -e '/Table_OBJ.php?A21&A6=/!d' > ${PAGE}

for i in ${LISTE}; do

    curl -b ${COOKIES} -sI "${i}" | grep "^Location" | sed -e 's/^Location: /http:\/\/genimmo.genest.lu\/FR\//'

done

rm -f ${COOKIES}
exit 0
