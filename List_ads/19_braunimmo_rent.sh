#!/bin/bash

SITE="braunimmo.lu"
TYPE="rt"

echo "${SITE}" >&2

PAGE=$(mktemp)
RESULTATS=$(mktemp)


curl "http://www.${SITE}/offer/search/transaction/${TYPE}/currentPage/1" -s -o ${PAGE}

NB_ADS=$(grep -A 1 '<div class="resume clearfix">' ${PAGE} | grep "annonces" | sed -e 's/^.* \([[:digit:]]\+\) annonces.*$/\1/' )

INDEX=2
NB_RESULT=0

cat ${PAGE} | tr '\r' ' ' | tr '\n' ' ' | sed -e '/class="detail"/!d' -e 's/class="detail"[[:space:]]*/\n/g' | sed -e '/^rel/!d' -e 's/rel="nofollow"[[:space:]]*href="//' | cut -d '"' -f 1 >>${RESULTATS}
NB_RESULT=$(cat ${RESULTATS} | wc -l )

while [ ${NB_RESULT} -lt ${NB_ADS} ]; do
    echo -n -e "\r${NB_RESULT}/${NB_ADS}" >&2
    curl "http://www.${SITE}/offer/search/transaction/${TYPE}/currentPage/${INDEX}" -s -o ${PAGE}
    cat ${PAGE} | tr '\r' ' ' | tr '\n' ' ' | sed -e '/class="detail"/!d' -e 's/class="detail"[[:space:]]*/\n/g' | sed -e '/^rel/!d' -e 's/rel="nofollow"[[:space:]]*href="//' | cut -d '"' -f 1 >>${RESULTATS}
    NB_RESULT=$(cat ${RESULTATS} | wc -l )
    INDEX=$((${INDEX}+1))
done
echo -e "\r${NB_RESULT}/${NB_ADS}" >&2

sed -e "s/^\//http:\/\/www.${SITE}\//" ${RESULTATS}

rm -f ${PAGE} ${RESULTATS}
exit 0
