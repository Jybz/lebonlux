#!/bin/bash

echo "weckbecker.lu" >&2

curl 'https://weckbecker.lu/properties/sales/' -s | sed -e '/^[[:space:]]*$/d' | grep -A 1 '<div class="more-button">' | grep 'href=' | sed -e 's/^.*href="/https:\/\/weckbecker.lu\//' -e 's/">.*//' #> ${PAGE}

exit 0
