#!/bin/bash

echo "mabro.lu" >&2

PAGE=$(mktemp)
curl 'https://mabro.lu/location/' -s -o ${PAGE}

sed -i -e '/<h2/!d' -e 's/<\/div><\/div><\/a><\/div>/\n/g' ${PAGE}
sed -i -e '/mabro.lu/!d' -e 's/^.*a href="//' -e 's/" class=.*$//'  ${PAGE}

cat ${PAGE}

rm -f ${PAGE}

exit 0
