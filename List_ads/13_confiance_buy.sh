#!/bin/bash

echo "confiance.lu" >&2

PAGE=$(mktemp)
RESULTATS=$(mktemp)


echo -n -e "\rx/x..." >&2
curl 'https://www.confiance.lu/annonces/vente/' -s -o ${PAGE} #| tr '\r' '\n' > ${PAGE}

PAGE_LIST=$(sed -e '/"page-numbers"/!d' ${PAGE} -e 's/^[[:space:]]*<a class="page-numbers" href="//' -e 's/">.*$//' )
LAST_PAGE=$(echo "${PAGE_LIST}" | tail -n1 | sed -e 's/^.*\/\([[:digit:]]*\)\//\1/' )

sed -e '/"btn btn-primary"/!d' -e 's/^[[:space:]]*<a href="//' -e 's/" class=".*$//' ${PAGE} >${RESULTATS}
# kwrite ${PAGE}

if [ -z "${LAST_PAGE}" ]; then
    echo -e "\r1/1      " >&2
else
    for i in $(seq 2 ${LAST_PAGE}); do
        echo -n -e "\rPage ${i}/${LAST_PAGE}..." >&2
        curl "https://www.confiance.lu/annonces/vente/${i}/" -s -o ${PAGE}
        sed -e '/"btn btn-primary"/!d' -e 's/^[[:space:]]*<a href="//' -e 's/" class=".*$//' ${PAGE} >>${RESULTATS}
    done
    echo -e "\rPage ${i}/${LAST_PAGE}..." >&2
fi


# echo -e "$(cat ${RESULTATS} | wc -l) ads." >&2
cat ${RESULTATS}

rm -f ${PAGE} ${RESULTATS}
exit 0
