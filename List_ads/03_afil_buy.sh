#!/bin/bash

echo "afil.lu" >&2

ADS_PER_PAGE=4

PAGE=$(mktemp)
TEMP=$(mktemp)
COOKIES=$(mktemp)

curl -c ${COOKIES} -b ${COOKIES} 'https://www.afil-luxembourg.lu/index.php?page=recherche&order=date_ajout_decroissant&recherche' --data 'vente_location=vente&zone=&type_de_bien=&nombre_chambres=&budget_max=' -s | iconv --from-code=ISO-8859-1 -c -t "UTF-8" | tr '\r' ' ' | tr '\n' ' ' | sed -e 's/[[:space:]]\+/ /g' > ${PAGE}

NB_RESULT=$(cat ${PAGE} | sed -e 's/^.*id="nb_resultats_text">//' -e 's/<\/b>.*//' -e 's/.*>\([[:digit:]]*\)$/\1/' )
CURRENT=0
INDEX=2

sed -i -e 's/<div class="annonce_bien/\n<div class="annonce_bien/g' ${PAGE}
sed -i -e '/<div class="annonce_bien/!d' ${PAGE}
sed -i -e 's/^.*style="position:relative;">[[:space:]]*<a href="/https:\/\/www.afil-luxembourg.lu\//' -e 's/">.*$//' ${PAGE}
cat ${PAGE} >${TEMP}
echo "" >>${TEMP}


CURRENT=$(cat ${TEMP} | wc -l)

while [ ${CURRENT} -lt ${NB_RESULT} ]; do
    echo -n -e "\r${CURRENT}/${NB_RESULT}" >&2
    curl -b ${COOKIES} "https://www.afil-luxembourg.lu/index.php?page=recherche&order=date_ajout_decroissant&recherche&paging=${INDEX}" -s | iconv --from-code=ISO-8859-1 -c -t "UTF-8" | tr '\r' ' ' | tr '\n' ' ' | sed -e 's/[[:space:]]\+/ /g' > ${PAGE}
    sed -i -e 's/<div class="annonce_bien/\n<div class="annonce_bien/g' ${PAGE}
    sed -i -e '/<div class="annonce_bien/!d' ${PAGE}
    sed -i -e 's/^.*style="position:relative;">[[:space:]]*<a href="/https:\/\/www.afil-luxembourg.lu\//' ${PAGE}
    sed -i -e 's/">.*$//' ${PAGE}
    cat ${PAGE} >>${TEMP}
    echo "" >>${TEMP}
    CURRENT=$(cat ${TEMP} | wc -l)
    INDEX=$((${INDEX}+1))
done
echo -e "\r${CURRENT}/${NB_RESULT}" >&2

cat ${TEMP}

rm -f ${PAGE} ${TEMP} ${COOKIES}

exit 0
