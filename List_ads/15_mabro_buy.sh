#!/bin/bash

echo "mabro.lu" >&2

PAGE=$(mktemp)
curl 'https://mabro.lu/vente/' -s -o ${PAGE} #| sed -e 's/<article/\n<article/g' -e 's/<\/article/\n<\/article/g' | grep "<article" | sed -e 's/^.*<a href="/http:\/\/www.rooftop.lu/' -e 's/">.*$//'

sed -i -e '/<h2/!d' -e 's/<\/div><\/div><\/a><\/div>/\n/g' ${PAGE}
sed -i -e '/mabro.lu/!d' -e 's/^.*a href="//' -e 's/" class=.*$//'  ${PAGE}

cat ${PAGE}

rm -f ${PAGE}

exit 0
