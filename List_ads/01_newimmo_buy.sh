#!/bin/bash

echo "newimmo.lu" >&2

PAGE=$(mktemp)
RESULTATS=$(mktemp)

curl 'https://www.newimmo.lu/fr/types-de-transactions/acheter' -s -o ${PAGE} 
NB_ADS=$(cat ${PAGE} | sed -e '/h1>/!d' -e 's/.*">\([[:digit:]]*\) .*$/\1/')
NB_ADS_PER_PAGE=$(cat ${PAGE} | grep "standarditem9" | wc -l)

INDEX=0
COUNT=1

while [ ${INDEX} -lt ${NB_ADS} ]; do
    echo -n -e "\r${INDEX}/${NB_ADS}" >&2
    curl "https://www.newimmo.lu/fr/types-de-transactions/acheter/${COUNT}" -H 'X-Requested-With: XMLHttpRequest' -s -o ${PAGE}
    COUNT=$((${COUNT}+1))
    INDEX=$((${INDEX}+$(cat ${PAGE} | grep "standarditem9" | wc -l) ))
    cat ${PAGE} >>${RESULTATS}
done
echo -n "${INDEX}/${NB_ADS}" >&2
echo -n -e "\r" >&2

cat ${RESULTATS} | sed -e 's/^[[:space:]]*//'| tr -d '\n' | tr -d '\r' | sed -e 's/--><li class="standarditem9">/\n--><li class="standarditem9">/g' >${PAGE}

sed -i -e 's/^--><li class="standarditem9"><div class="standardlink9_wrapper"><a href="/https:\/\/newimmo.lu/' -e 's/class="[[:space:]]*standardlink9 cl1 bd1"//g' -e 's/" >.*//' -e '/newimmo.lu/!d' ${PAGE}

# kwrite ${RESULTATS}
cat ${PAGE}
echo "" >&2

rm -f ${PAGE} ${RESULTATS}

exit 0
