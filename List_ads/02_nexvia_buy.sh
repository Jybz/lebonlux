#!/bin/bash

echo "Nexvia.lu" >&2

PAGE=$(mktemp)
TEMP=$(mktemp)

curl 'https://www.nexvia.lu/buy' -s -o ${PAGE}

sed -i -e '/window\.internalTranslations/d' -e '/^[[:space:]]*$/d' -e 's/^[[:space:]]*//' ${PAGE}

cat ${PAGE} | tr '\n' ' ' >${TEMP}
# sed -i -e 's/<\/a>/<\/a>\n/g' ${TEMP}
sed -i -e 's/<a class="col-md-6 listings-item-wrapper"/\n<a class="col-md-6 listings-item-wrapper"/g' ${TEMP}
sed -i -e 's/<\/span> <\/div> <\/div> <\/div> <\/a>.*$//' ${TEMP}
sed -i -e '/class="col-md-6 listings-item-wrapper"/!d' ${TEMP}
sed -i -e '/<span class="listings-item-state"> $/!d' ${TEMP}

sed -i -e 's/<a class="col-md-6 listings-item-wrapper" href="//' -e 's/"> <div class="listings-item">.*$//' ${TEMP}

cat ${TEMP}

rm -f ${PAGE} ${TEMP}
