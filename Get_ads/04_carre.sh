#!/bin/bash

# 0_INFO
# 1_REF
# 2_DATE
# 3_TITLE
# 4_DESCRIPTION
# 5_PRICE
# 6_SURFACE
# 7_IMAGE_LIST
# 8_LOST

SITE="carre"
ROOT_PATH=$(echo $(pwd))

ADS_URL="${1}"
echo "${ADS_URL}" >&2

PAGE=$(mktemp)

curl "${ADS_URL}" -s > ${PAGE}

if [ ${?} -ne 0 ]; then
    curl "${ADS_URL}" -s > ${PAGE}
fi

if [ ${?} -ne 0 ]; then
    echo "Error while downloading... Exiting." >&2
    rm -f ${PAGE}
    exit 0
else

    REF_ADS="$(sed -e '/bien_id/!d' -e 's/^.*value="//' -e 's/".*$//' ${PAGE} )"
    AD_PATH="${ROOT_PATH}/ads/04_${SITE}/${REF_ADS}"
    
    if [ ! -d ${AD_PATH} ]; then
        mkdir -p "${AD_PATH}"
        cd "${AD_PATH}"
        
        TITLE="$(sed -e '/name="bien_name"/!d' -e 's/^.*value="//' -e 's/"[[:space:]]*\/>[[:space:]]*$//' -e 's/&sup2;/²/' ${PAGE} )"

        PRICE="$(sed -e '/class="price-block"/!d' -e 's/^[[:space:]]*<span class="price-block">//' -e 's/<\/span>.*//' ${PAGE} )"
        SURFACE="$(sed -e '/Surface area:/!d' -e 's/^.*Surface area:[[:space:]]*//' -e 's/m.*$/m²/' ${PAGE})"
        LIST_IMAGE="$(sed -e '/class="slider-thumbnails-wrapper"/,/^<\/div>[[:space:]]*$/!d' -e '/<li><img src="/!d' -e 's/^[[:space:]]*<li><img src="//' -e 's/" alt="" \/><\/li>[[:space:]]*//' ${PAGE} )"
        DESCRIPTION="$( sed -e '/<div class="row-description">/,/<div class="block-form">/!d' -e '1d' ${PAGE} )"

        INFO="$(sed -e '/property_print_header/,/class="row-related/!d' ${PAGE} )"


        
        if [ -e "${AD_PATH}/7_IMAGE_LIST" ]; then
            rm "${AD_PATH}/7_IMAGE_LIST"
        fi
        for i in ${LIST_IMAGE}; do
            FILE_NAME=$(echo ${i} | sed -e 's/.*\/\([-\.[:alnum:]]*\)/7_IMG_\1/' )
            curl -C - -s ${i} -o "${AD_PATH}/${FILE_NAME}"
            echo "${FILE_NAME}" >> "${AD_PATH}/7_IMAGE_LIST"
        done
        
        echo "${INFO}" > "${AD_PATH}/0_INFO"
        echo -e "${ADS_URL}\n${REF_ADS}" >> "${AD_PATH}/1_REF"
        echo "$(date '+%Y-%m-%d_%H:%M:%S')" >> "${AD_PATH}/2_DATE"
        echo "${TITLE}" > "${AD_PATH}/3_TITLE"
        echo -e "${DESCRIPTION}" > "${AD_PATH}/4_DESCRIPTION"
        echo "${PRICE}" > "${AD_PATH}/5_PRICE"
        echo "${SURFACE}" > "${AD_PATH}/6_SURFACE"

        echo "${AD_PATH}"
    fi
    
    rm -f ${PAGE}
fi
exit 0

