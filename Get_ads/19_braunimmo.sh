#!/bin/bash

echo "braunimmo.lu" >&2

# 0_INFO
# 1_REF
# 2_DATE
# 3_TITLE
# 4_DESCRIPTION
# 5_PRICE
# 6_SURFACE
# 7_IMAGE_LIST
# 8_LOST


SITE="braunimmo"
SCRIPTNB='19'
ROOT_PATH=$(echo $(pwd))

ADS_URL="${1}"
echo "${ADS_URL}" >&2

PAGE=$(mktemp)

curl "${ADS_URL}" -s > ${PAGE}

if [ ${?} -ne 0 ]; then
    echo "Error while downloading... Exiting." >&2
    rm -f ${PAGE}
    exit 0
else

    INFO=$(sed -e '/initGoogleMap/!d' -e 's/^.*initGoogleMap(//' -e 's/, \$.*$//' ${PAGE} )
    if [ -z "${INFO}" ]; then
        echo "Using legacy HTML parsing method..." >&2
        REF_ADS="$(sed -e '/name="offerid"/!d' -e 's/^.*value="//' -e 's/".*$//' ${PAGE} )"
        AD_PATH="${ROOT_PATH}/ads/${SCRIPTNB}_${SITE}/${REF_ADS}"
        
        if [ ! -d ${AD_PATH} ]; then
            mkdir -p "${AD_PATH}"
            cd "${AD_PATH}"
            
            TITLE="$(sed -e '/"og:title"/!d' -e 's/^.*content="//' -e 's/" >.*$//' ${PAGE} )"

            PRICE="$(sed -e '/__detail_selling_price/!d' -e 's/^.*class="value">//' -e 's/[[:space:]]*//g' -e 's/ //g' -e 's/[[:space:]]*&.*$//' ${PAGE} )"
            SURFACE="$(sed -e '/living_surface/!d' -e 's/^.*class="value">//' -e 's/[[:space:]]*//g' -e 's/m.*$//' ${PAGE} )"
            LIST_IMAGE="$(sed -e '/onmouseover/!d' -e 's/^.*href="//' -e 's/".*$//' ${PAGE} )"
            INFO="$(sed -e '/<div class="description">/,/<div class="form_contact">/!d' ${PAGE} )"
            DESCRIPTION="$(sed -e '/<div class="description">/,/<div class="form_contact">/!d' -e '1,/<h2>Ca/!d' ${PAGE} )"
            
            if [ ! -z "${LIST_IMAGE}" ]; then
                if [ -e "${AD_PATH}/7_IMAGE_LIST" ]; then
                    rm "${AD_PATH}/7_IMAGE_LIST"
                fi
                
                for i in ${LIST_IMAGE}; do
                    FILE_NAME=$(echo ${i} | sed -e 's/.*\/\([-\.[:alnum:]]*\)/7_IMG_\1/' )
                    curl -C - -s ${i} -o "${AD_PATH}/${FILE_NAME}"
                    echo "${FILE_NAME}" >> "${AD_PATH}/7_IMAGE_LIST"
                done
            else
                echo "No pictues." >&2
            fi
            
            echo "${INFO}" > "${AD_PATH}/0_INFO"
            echo -e "${ADS_URL}\n${REF_ADS}" >> "${AD_PATH}/1_REF"
            echo "$(date '+%Y-%m-%d_%H:%M:%S')" >> "${AD_PATH}/2_DATE"
            echo "${TITLE}" > "${AD_PATH}/3_TITLE"
            echo -e "${DESCRIPTION}" > "${AD_PATH}/4_DESCRIPTION"
            echo "${PRICE}" > "${AD_PATH}/5_PRICE"
            echo "${SURFACE}" > "${AD_PATH}/6_SURFACE"

            echo "${AD_PATH}"
        fi
    else
        REF_ADS="$(echo "${INFO}" | jq -r '.id' )"
        AD_PATH="${ROOT_PATH}/ads/${SCRIPTNB}_${SITE}/${REF_ADS}"
        
        if [ ! -d ${AD_PATH} ]; then
            mkdir -p "${AD_PATH}"
            cd "${AD_PATH}"
            
            TITLE="$(sed -e '/"og:title"/!d' -e 's/^.*content="//' -e 's/" >.*$//' ${PAGE} )"

            PRICE="$(echo "${INFO}" | jq -r '.price' )"
            SURFACE="$(echo "${INFO}" | jq -r '.surface' )"
            LIST_IMAGE="$(echo "${INFO}" | jq -r 'select( .pictures ) | .pictures[].large' )"
            DESCRIPTION="$(echo "${INFO}" | jq -r '.description' )"
            
            if [ ! -z "${LIST_IMAGE}" ]; then
                if [ -e "${AD_PATH}/7_IMAGE_LIST" ]; then
                    rm "${AD_PATH}/7_IMAGE_LIST"
                fi
                
                for i in ${LIST_IMAGE}; do
                    FILE_NAME=$(echo ${i} | sed -e 's/.*\/\([-\.[:alnum:]]*\)/7_IMG_\1/' )
                    curl -C - -s ${i} -o "${AD_PATH}/${FILE_NAME}"
                    echo "${FILE_NAME}" >> "${AD_PATH}/7_IMAGE_LIST"
                done
            else
                echo "No pictues." >&2
            fi
            
            echo "${INFO}" > "${AD_PATH}/0_INFO"
            echo -e "${ADS_URL}\n${REF_ADS}" >> "${AD_PATH}/1_REF"
            echo "$(date '+%Y-%m-%d_%H:%M:%S')" >> "${AD_PATH}/2_DATE"
            echo "${TITLE}" > "${AD_PATH}/3_TITLE"
            echo -e "${DESCRIPTION}" > "${AD_PATH}/4_DESCRIPTION"
            echo "${PRICE}" > "${AD_PATH}/5_PRICE"
            echo "${SURFACE}" > "${AD_PATH}/6_SURFACE"

            echo "${AD_PATH}"
        fi
    fi
    
    rm -f ${PAGE}
fi
exit 0
