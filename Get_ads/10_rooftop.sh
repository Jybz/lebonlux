#!/bin/bash

echo "rooftop.lu" >&2

curl 'http://www.rooftop.lu/projects/a-louer.html' -s | sed -e 's/<article/\n<article/g' -e 's/<\/article/\n<\/article/g' | grep "<article" | sed -e 's/^.*<a href="/http:\/\/www.rooftop.lu/' -e 's/">.*$//'

exit 0
