#!/bin/bash

echo "immostoffel.lu" >&2

# 0_INFO
# 1_REF
# 2_DATE
# 3_TITLE
# 4_DESCRIPTION
# 5_PRICE
# 6_SURFACE
# 7_IMAGE_LIST
# 8_LOST


SITE="immostoffel"
ROOT_PATH=$(echo $(pwd))

ADS_URL="${1}"
echo "${ADS_URL}" >&2

PAGE=$(mktemp)

curl "${ADS_URL}" -s > ${PAGE}

if [ ${?} -ne 0 ]; then
    echo "Error while downloading... Exiting." >&2
    rm -f ${PAGE}
    exit 0
else

    INFO=$(sed -e '/initGoogleMap/!d' -e 's/^.*initGoogleMap(//' -e 's/, \$.*$//' ${PAGE} )
    REF_ADS="$(echo "${INFO}" | jq -r '.id' )"
    AD_PATH="${ROOT_PATH}/ads/11_${SITE}/${REF_ADS}"
    
    if [ ! -d ${AD_PATH} ]; then
        mkdir -p "${AD_PATH}"
        cd "${AD_PATH}"
        
        TITLE="$(sed -e '/"og:title"/!d' -e 's/^.*content="//' -e 's/" >.*$//' ${PAGE} )"

        PRICE="$(echo "${INFO}" | jq -r '.price' )"
        SURFACE="$(echo "${INFO}" | jq -r '.surface' )"
        LIST_IMAGE="$(echo "${INFO}" | jq -r 'select( .pictures ) | .pictures[].large' )"
        DESCRIPTION="$(echo "${INFO}" | jq -r '.description' )"
        
        if [ ! -z "${LIST_IMAGE}" ]; then
            if [ -e "${AD_PATH}/7_IMAGE_LIST" ]; then
                rm "${AD_PATH}/7_IMAGE_LIST"
            fi
            
            for i in ${LIST_IMAGE}; do
                FILE_NAME=$(echo ${i} | sed -e 's/.*\/\([-\.[:alnum:]]*\)/7_IMG_\1/' )
                curl -C - -s ${i} -o "${AD_PATH}/${FILE_NAME}"
                echo "${FILE_NAME}" >> "${AD_PATH}/7_IMAGE_LIST"
            done
        else
            echo "No pictues." >&2
        fi
        
        echo "${INFO}" > "${AD_PATH}/0_INFO"
        echo -e "${ADS_URL}\n${REF_ADS}" >> "${AD_PATH}/1_REF"
        echo "$(date '+%Y-%m-%d_%H:%M:%S')" >> "${AD_PATH}/2_DATE"
        echo "${TITLE}" > "${AD_PATH}/3_TITLE"
        echo -e "${DESCRIPTION}" > "${AD_PATH}/4_DESCRIPTION"
        echo "${PRICE}" > "${AD_PATH}/5_PRICE"
        echo "${SURFACE}" > "${AD_PATH}/6_SURFACE"

        echo "${AD_PATH}"
    fi
    
    rm -f ${PAGE}
fi
exit 0
