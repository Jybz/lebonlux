#!/bin/bash

# 0_INFO
# 1_REF
# 2_DATE
# 3_TITLE
# 4_DESCRIPTION
# 5_PRICE
# 6_SURFACE
# 7_IMAGE_LIST
# 8_LOST

SITE="engelvoelkers"
ROOT_PATH=$(echo $(pwd))

ADS_URL="${1}"
echo "${ADS_URL}" >&2

PAGE=$(mktemp)
TMP=$(mktemp)

# curl "${ADS_URL}" -s
# cat ./TMP | 
curl -L "${ADS_URL}" -s -o ${TMP}
if [ ${?} -ne 0 ]; then
    echo "Error while downloading... Exiting." >&2
    rm -f ${PAGE} ${TMP}
    exit 0
else
    cat ${TMP} | iconv --from-code=ISO-8859-1 -c -t "UTF-8" | sed -e 's/&amp;/&/g' -e "s/&rsquo;/'/g" -e 's/&eacute;/é/g' -e 's/&ucirc;/û/g' -e 's/&acirc;/â/g' -e 's/&agrave;/à/g' -e 's/&egrave;/è/g' -e 's/&ccedil;/ç/g' -e 's/&ldquo;/«/g' -e 's/&rdquo;/»/g' -e 's/&ecirc;/ê/g' -e 's/&plusmn;/±/g' -e 's/<\/p>//g' -e 's/&ouml;/ö/g' -e 's/&sup2;/²/g' > ${PAGE}

    sed -i -e 's/<\/\([[:alnum:]]*\)>/<\/\1>\n/g' -e 's/<meta/\n<meta/g' -e 's/<\/div>/\n<\/div>/g' ${PAGE}
    REF_ADS=$(sed -e '/"displayId hidden"/!d' -e 's/^.*<li>//' -e 's/<\/li>.*$//' ${PAGE} )
    
    AD_PATH="${ROOT_PATH}/ads/00_${SITE}/${REF_ADS}"
    if [ ! -d ${AD_PATH} ]; then
        mkdir -p "${AD_PATH}"
        cd "${AD_PATH}"

        INFO=$(grep -m 1 'ev-teaser-icon ev-watchlist-icon inactive' ${PAGE} | sed -e 's/^.*event, {/{/' -e 's/);">.*$//' -e 's/\([_[:alnum:]]\+\): /"\1": /g' \
        -e "s/'\''/\"/g" -e 's/\\//g' \
        -e "s/'[[:space:]]*:/\" :/g" -e "s/:[[:space:]]*'/: \"/g" \
        -e "s/'[[:space:]]*,/\", /g" -e "s/,[[:space:]]*'/, \"/g" \
        -e "s/{[[:space:]]*'/{ \"/g" -e "s/'[[:space:]]*{/\" {/g" \
        -e "s/}[[:space:]]*'/} \"/g" -e "s/'[[:space:]]*}/\" }/g" \
        )
    #     -e "s/':'/\":\"/g" ) # -e "s/'/\"/g" )

        echo "${INFO}" | jq >/dev/null
        if [ ${?} -ne 0 ]; then
            echo "${INFO}" >&2
        fi
        
        TITLE=$(echo "${INFO}" | jq -r '.title_' )
        PRICE=$(echo "${INFO}" | jq -r '.watchValues.price.value' )
        SURFACE=$(echo "${INFO}" | jq -r '.watchValues.keyFacts[] | select(.label == "LivingSpace").value ' )
        
        LIST_IMAGE=$(sed -e '/"ev-image-gallery-image-link "/!d' -e 's/^.*href="//' -e 's/?.*$//' -e 's/[-_%[:alnum:]]\+$//' ${PAGE})
        # luxueux-appartement-meubl%C3%A9-%C3%A0-limpertsberg


        DESCRIPTION=$(grep 'class="ev-exposee-text"' ${PAGE} | sed -e 's/<hr class="ev-exposee-detail-spacer"\/>/\n<hr class="ev-exposee-detail-spacer"\/>/' )
        DESCRIPTION_GOOD=$(echo "${DESCRIPTION}" | sed -e '/<p class="ev-exposee-text" itemprop="description">/!d' -e 's/<br \/>/\n/g' -e 's/<[-= "\/[:alnum:]]*>//g')
        DESCRIPTION_PLACE=$(echo "${DESCRIPTION}" | sed -e '/ <p class="ev-exposee-text" itemprop="description">/d' -e 's/<br \/>/\n/g' -e 's/<[-= "\/[:alnum:]]*>//g' )
        
        if [ -e "${AD_PATH}/7_IMAGE_LIST" ]; then
            rm "${AD_PATH}/7_IMAGE_LIST"
        fi
        for i in ${LIST_IMAGE}; do
            FILE_NAME=$(echo ${i} | sed -e 's/.*\/\([-[:alnum:]]*\)\//\1/' )
            curl -C - -s ${i} -o "${AD_PATH}/${FILE_NAME}"
            echo "${FILE_NAME}" >> "${AD_PATH}/7_IMAGE_LIST"
        done
        
        echo "${INFO}" > "${AD_PATH}/0_INFO"
        echo -e "${ADS_URL}\n${REF_ADS}" >> "${AD_PATH}/1_REF"
        echo "$(date '+%Y-%m-%d_%H:%M:%S')" >> "${AD_PATH}/2_DATE"
        echo "${TITLE}" > "${AD_PATH}/3_TITLE"
        echo -e "${DESCRIPTION_GOOD}\n\n${DESCRIPTION_PLACE}" > "${AD_PATH}/4_DESCRIPTION"
        echo "${PRICE}" > "${AD_PATH}/5_PRICE"
        echo "${SURFACE}" > "${AD_PATH}/6_SURFACE"

        echo "${AD_PATH}"
    fi
    
    rm -f ${PAGE} ${TMP}
fi
exit 0
