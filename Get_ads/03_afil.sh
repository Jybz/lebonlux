#!/bin/bash

# 0_INFO
# 1_REF
# 2_DATE
# 3_TITLE
# 4_DESCRIPTION
# 5_PRICE
# 6_SURFACE
# 7_IMAGE_LIST
# 8_LOST

SITE="afil"
ROOT_PATH=$(echo $(pwd))

ADS_URL=$(echo "${1}" | sed -e 's/afil-luxembourg.lu\/\([[:digit:]]*\)-.*$/afil-luxembourg.lu\/\1-blabla/')
ADS_URL_ORI="${1}"

PAGE=$(mktemp)

echo "${ADS_URL}" >&2 

curl -L "${ADS_URL}" -s > ${PAGE}
if [ ${?} -ne 0 ]; then
    curl -L "${ADS_URL}" -s > ${PAGE}
fi

if [ ${?} -ne 0 ]; then
    echo "Error while downloading... Exiting." >&2
    rm -f ${PAGE}
    exit 0
else
    REF_ADS="$(grep -m 1 'lien_langue' ${PAGE} | sed -e 's/^.*id=\([[:alnum:]]*\)&.*$/\1/')"

    AD_PATH="${ROOT_PATH}/ads/03_${SITE}/${REF_ADS}"
    if [ ! -d ${AD_PATH} ]; then
        mkdir -p "${AD_PATH}"
        cd "${AD_PATH}"
        
        
        TITLE="$(grep "<title>" ${PAGE} | sed -e 's/^[[:space:]]*<title>//' -e 's/<\/title>[[:space:]]*//' )"

        PRICE="$(sed -e '/detail_prix/,/<br>/!d' -e '/detail_prix/d' -e 's/^[[:space:]]*\([[:digit:]]*[[:space:]]*[[:digit:][:space:]]*\).*$/\1/' -e 's/[[:space:]]*//g' ${PAGE})"
        SURFACE="$(sed -e '/detail_listing/,/<\/span>/!d' -e '/span/!d' -e 's/^[[:space:]]*<span class="">\([\.[:digit:]]*\) .*$/\1/' ${PAGE})" # <span class="">82.00 
        
        LIST_IMAGE="$(grep 'detail_img' ${PAGE} | sed -e 's/^.*src="/https:\/\/www.afil-luxembourg.lu\//' -e 's/" alt=.*$//')"
        DESCRIPTION="$(grep -A 1 "bloc_texte_detail" ${PAGE} | sed -e 's/^[[:space:]]*<div id="bloc_texte_detail"[-"=[:space:][:alnum:]]*">//' -e 's/<\/div>//' -e 's/<br>/\n/g' )"

        INFO="$(sed -e '/specifications_details/,$!d' -e '/^[[:space:]]*<.table>/,$d' ${PAGE} )"

        
        if [ -e "${AD_PATH}/7_IMAGE_LIST" ]; then
            rm "${AD_PATH}/7_IMAGE_LIST"
        fi
        for i in ${LIST_IMAGE}; do
            FILE_NAME=$(echo ${i} | sed -e 's/.*\/\([-\.[:alnum:]]*\)/\1/' )
            curl -C - -s ${i} -o "${AD_PATH}/${FILE_NAME}"
            echo "${FILE_NAME}" >> "${AD_PATH}/7_IMAGE_LIST"
        done
        
        echo "${INFO}" > "${AD_PATH}/0_INFO"
        echo -e "${ADS_URL_ORI}\n${REF_ADS}" >> "${AD_PATH}/1_REF"
        echo "$(date '+%Y-%m-%d_%H:%M:%S')" >> "${AD_PATH}/2_DATE"
        echo "${TITLE}" > "${AD_PATH}/3_TITLE"
        echo -e "${DESCRIPTION}" > "${AD_PATH}/4_DESCRIPTION"
        echo "${PRICE}" > "${AD_PATH}/5_PRICE"
        echo "${SURFACE}" > "${AD_PATH}/6_SURFACE"

        echo "${AD_PATH}"
        
    fi
    rm -f ${PAGE}
fi
exit 0
